import { Component,OnInit,VERSION,NgZone  } from "@angular/core";
import {AsubService} from './asub.service';
import {GlobalState} from './global.state';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
  } from '@angular/router'
import { setInterval, setTimeout } from "core-js/library/web/timers";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public articles:any=[];
    public option:any={size:10,page:0,type:""};
    public showPub=true;
    public option1:any={size:10,page:0,type:"publicité"};
    public publicites:any={};
constructor(public router: Router,public service:AsubService,public globalState:GlobalState,public zone: NgZone){
    console.log(VERSION.full);
    
}
    ngOnInit(){
        this.router.events.subscribe((event: RouterEvent) => {
            if (event instanceof NavigationEnd) {
              if(event.url=='/actualite'){
               this.showPub=true;
               this.getPub();
               if(this.publicites.length==0){
                
               }
              }
              else{
                  this.showPub=false;
              }
              }
          })
    }
    getPub(){
        this.service.getPublicites().subscribe(res=>{
            this.publicites=res;
            this.publicites.map(el=>{
                    el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
            })
            this.dispatchPub();
        })
        
    }
    dispatchPub(){
        if(this.publicites.length>0){
            let i=0;
            this.globalState.notifyDataChanged("pub",this.publicites[i]);
            this.zone.runOutsideAngular(() => {
            setInterval(()=>{
                i++;
                if(i<this.publicites.length){
                    this.globalState.notifyDataChanged("pub",this.publicites[i]);
                }
                else{
                    i=0;
                }
            },5000)
            });
        }
        else{
            console.log("rien")
        }
    }
}

