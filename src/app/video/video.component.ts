/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit,HostListener } from "@angular/core";
import { DomSanitizer } from '@angular/platform-browser';
import {AsubService} from './../asub.service';
import {GlobalState} from '../global.state';
import * as $ from 'jquery';


@Component({
    selector:'video-frame',
    styleUrls: ['./video.component.scss'],
    templateUrl: './video.component.html',
})
export class VideoComponent implements OnInit {
public video;
public width="100%";
public url="https://www.youtube.com/embed/";
constructor(public globalState:GlobalState,public sanitizer: DomSanitizer){
    console.log("video")
   
}

ngOnInit(){
    this.globalState.subscribe("display",data=>{
        this.video=data;
        this.url="https://www.youtube.com/embed/";
        this.url+=this.video.youtubeVideoId;        
        console.log(this.video);
        this.display();
    })
}
setHeight(){
    if(window.matchMedia( "(min-width: 765px)" ).matches){
        this.width="100%";
    }
    else{
        this.width="80%";
    }
}
@HostListener('window:resize', ['$event'])
onResize(event) {
    this.setHeight();
}
display(){
    console.log("display");
    $("#video-div").animate({
        left: '0',
        right:'0',
        top: '30px',
        height: '95%',
        marginLeft:'-10px',
        width: this.width,  
        display : 'block',      
    });
}
close(){
    $("#video-div").animate({
        top: '100%',
        height: '0px',
        width: this.width,  
        display : 'none',      
    });
    this.url="https://www.youtube.com/embed/";
}
}
