/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit } from "@angular/core";
import {AsubService} from './../asub.service';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
  } from '@angular/router';
  import * as $ from 'jquery';
@Component({
    styleUrls: ['./evenements.component.scss'],
    templateUrl: './evenements.component.html',
})
export class EvenementsComponent implements OnInit {
    public evenements:any={content:[]};
    public option:any={size:10,page:0,type:"",categorie:"",pays:"",titre:"",lieu:""};    
    public baseImageUrl;
constructor(public router: Router,public service:AsubService){
    this.router.events.subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
            console.log(this.evenements.content);
          if(this.evenements.content.length==0 &&event.url=='/evenements'){
            $('html, body').animate({scrollTop:0}, {duration:1000});
            console.log(event.url)
            console.log(event.id);
           this.getEvenements();
          }
          }
      })
}
getEvenements(){
    this.service.getEvenements(this.option).subscribe(res=>{
        this.evenements=res;
        this.evenements.content.map((el,index)=>{
            el.date = new Date(el.date);
            el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
        })
    })
}
more(){
    this.option.size=this.option.size*2;
    this.getEvenements();
}
ngOnInit(){
   
}
view(id){
    this.router.navigate(['/evenement',id]);
}
}
