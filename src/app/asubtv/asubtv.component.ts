/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit } from "@angular/core";
import {AsubService} from './../asub.service';
import {GlobalState} from '../global.state';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
  } from '@angular/router';
  import * as $ from 'jquery';
@Component({
    styleUrls: ['./asubtv.component.scss'],
    templateUrl: './asubtv.component.html',
})
export class AsubTvComponent implements OnInit {
    public articles:any={content:[]};
    public auteur;
    public option:any={size:10,page:0,types:["Vidéo"],categorie:"",pays:"",titre:"",id:0};
    public baseImageUrl;
constructor(public router: Router,public service:AsubService,public globalState:GlobalState){
    this.router.events.subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
            console.log(this.articles.content);
          if(this.articles.content.length==0 &&event.url=='/asubtv'){
            $('html, body').animate({scrollTop:0}, {duration:1000});
            console.log(event.url)
            console.log(event.id);
           this.getArticles();
          }
          }
      })
}
getArticles(){
    this.service.getArticles(this.option).subscribe(res=>{
        this.articles=res;
        this.articles.content.map(el=>{
            if(el.contenu.length>100){
                el.contenu=el.contenu.substring(0, 101);
                el.contenu=el.contenu+"...";
                
            }
            el.photoUrl=el.photoUrl?"http://193.70.39.157:8080/asub/image?photo="+el.photoUrl:"http://img.youtube.com/vi/"+el.youtubeVideoId+"/maxresdefault.jpg";
        console.log(el.photoUrl)
        })
    })
}
more(){
    this.option.size=this.option.size*2;
    this.getArticles();
}
ngOnInit(){
    this.globalState.subscribe("videos-with-auth",data=>{
        console.log(data)
        this.auteur=data;
        this.option.id=data.id;
        this.getArticles();
    })
 }
 deleteAuth(){
 this.option.id=0;
 this.auteur=undefined;
 this.getArticles();
 }
play(video){
    console.log("play")
    this.globalState.notifyDataChanged("display",video);
}
search(){
    this.getArticles();
}
}
