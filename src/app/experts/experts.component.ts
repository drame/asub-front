/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit } from "@angular/core";
import {AsubService} from './../asub.service';
import {GlobalState} from '../global.state';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
  } from '@angular/router'
@Component({
    styleUrls: ['./experts.component.scss'],
    templateUrl: './experts.component.html',
})
export class ExpertsComponent implements OnInit {
    public users:any=[];
    public baseImageUrl;
constructor(public router: Router,public service:AsubService,public globalState:GlobalState){
    this.router.events.subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          if(this.users.length==0 &&event.url=='/experts'){
           this.getExperts();
          }
          }
      })
}
getExperts(){
    this.service.getExperts().subscribe(res=>{
        this.users=res;
    })

}
viewArticles(a){
    this.globalState.notifyDataChanged("articles-with-auth",a);
    this.router.navigate(['/entrep_startup']);
    
}
viewOpinions(a){
    this.globalState.notifyDataChanged("opinions-with-auth",a);
    this.router.navigate(['/opinions']);

}
viewVideos(a){
    this.globalState.notifyDataChanged("videos-with-auth",a);
    this.router.navigate(['/asubtv']);

}
ngOnInit(){
   
}

}
