/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit } from "@angular/core";
import {AsubService} from './../asub.service';
import {GlobalState} from '../global.state';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,ActivatedRoute
  } from '@angular/router';
  import * as $ from 'jquery';
@Component({
    styleUrls: ['./article.component.scss'],
    templateUrl: './article.component.html',
})
export class ArticleComponent implements OnInit {
    public article:any={};
    public articles:any=[];
    public data:any={};
    public option:any={size:2,page:0,type:"",categorie:"",pays:"",titre:""};
    public evenements:any={};
    public baseImageUrl;
    public pub;
    public palmares:any=[];
constructor(public router: Router,public service:AsubService,public activatedRoute: ActivatedRoute,public globalState:GlobalState){

}

ngOnInit(){
   this.activatedRoute.params.subscribe(params=>{
       let id=params["id"];
       if(id>0){
           this.service.getArticle(id).subscribe(res=>{
            $('html, body').animate({scrollTop:0}, {duration:1000});
               if(res.titre){
                   this.article=res;
                   this.article.photoUrl="http://193.70.39.157:8080/asub/image?photo="+this.article.photoUrl;
                   this.data.id=this.article.id;
                   this.data.categorie=this.article.categorie;
                   this.data.pays=this.article.pays;
                   this.data.type=this.article.type;
                   this.data.categorie=this.article.categorie;
                   this.getArticlesAssocie();
                  // this.getEvenementsAssocie();
               }
               else{
                   this.router.navigate(['/actualite']);
               }
           })
           this.getPalmares();
       }
   })
   this.globalState.subscribe('pub',data=>{
       console.log("pub",data)
    this.pub=data;
})
}
view(id){
    this.router.navigate(['/article',id]);
}
getArticlesAssocie(){
this.service.getArticlesAssocie(this.data).subscribe(res=>{
    this.articles=res;
    this.articles.map(el=>{
            el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
    })
});
}
getEvenementsAssocie(){
this.service.getEvenementsAssocie(this.option).subscribe(res=>{
    this.evenements=res;
    this.evenements.content.map(el=>{
            el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
    })
});
}
more(){
    this.option.size=this.option.size*2;
    this.getEvenementsAssocie();
}
arrondVar(v){
    return Math.round(v*10000)/100;
  }
  
  getPalmares(){
    this.service.getPalmares().subscribe(res=>{

        this.palmares=res;
    })
}
}
