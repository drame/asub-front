import { Injectable } from '@angular/core';
import { Http,Headers,Response,RequestOptions} from '@angular/http';
import {baseUrl} from './config';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class AsubService {
  public headers;
  constructor(public http:Http) { }
  // structures
  getStructures1(){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    
    let options = new RequestOptions({ headers: this.headers });
    let url=baseUrl+"/structures/get";
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  getStructure(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    
    let options = new RequestOptions({ headers: this.headers });
    let url=baseUrl+"/structure?s="+data;
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  
  // donnees
  getDonnees(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://193.70.39.157:4200');
    let options = new RequestOptions({ headers: this.headers });
    let url=baseUrl+"/donnees?size="+data.size+"&page="+data.page+"&s="+data.symboleStructure;
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  getAllDonneesByStructure(s){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://193.70.39.157:4200');
    let options = new RequestOptions({ headers: this.headers });
    let url=baseUrl+"/donnees/get?s="+s;
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
 
getArticles(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/articles/criteria?id="+data.id+"&page="+data.page+"&size="+data.size+"&titre="+data.titre+"&categorie="+data.categorie+"&pays="+data.pays;
  return this.http.post(url,data.types,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getArticlesOfStructure(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/articles/structure?s="+data;
  return this.http.post(url,data.types,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getTaux(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/taux"
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getEvenements(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/evenements/criteria?type="+data.type+"&page="+data.page+"&size="+data.size+"&categorie="+data.categorie+"&fin="+data.fin+"&titre="+data.titre+"&lieu="+data.lieu;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getEvenementsAssocie(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/evenements/associe?type="+data.type+"&categorie="+data.categorie+"&page="+data.page+"&size="+data.size;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}

getArticle(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/article/get?id="+data;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getExperts(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/users/get";
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getEvenement(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/evenement/get?id="+data;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getArticlesAssocie(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/articles/associe?id="+data.id+"&categorie="+data.categorie+"&pays="+data.pays+"&type="+data.type;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getPublicites(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://193.70.39.157:4200');
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/publicites/get"
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getStructures(brvm){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://193.70.39.157:4200');
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/structures/get?brvm="+brvm
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getPalmares(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://193.70.39.157:4200');
  let options = new RequestOptions({ headers: this.headers });
  let url=baseUrl+"/palmares"
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}

}
