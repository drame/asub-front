/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,NgZone } from '@angular/core';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import {AsubService} from './../asub.service';
import { Observable } from 'rxjs/Rx';
import * as $ from 'jquery';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
  } from '@angular/router'
import { setTimeout, Array } from 'core-js/library/web/timers';
import { Data } from '@angular/router/src/config';
import * as moment from 'moment';
@Component({
    styleUrls: ['./marche.component.scss'],
    templateUrl: './marche.component.html'
})
export class MarcheComponent {
    public articles:any=[];
    public option:any={size:10,page:0,type:""};
    public baseImageUrl;
    open: Boolean = false;
    public options: any;
    public chart2: AmChart;
    public timer: any;
    public chartData1 = [];
    public chartData2 = [];
    public chartData3 = [];
    public chartData4 = [];
    public structures=[];
    public brvm;
    public structure:any;
    public prevDataSets:any;
    public dashboardActive=true;
    public entrepriseActive=false;
    public analyseActive=false;
    public actualitedActive=false;
    public brvmActive=false;
    public donneesCourantes=[];
    public palmares={baisses:[],hausses:[]};
  
    constructor(public AmCharts: AmChartsService,public router: Router,public service:AsubService,public zone: NgZone) {
      
    }

    generateChartData(symbole) {
      let datas=[];
      let resultat;
       return this.service.getAllDonneesByStructure(symbole).map(res=>{
        resultat=res;
        res.forEach(element => {
          
          let data={date:element.date,value:element.cours_prec,volume:element.vol_titre?element.vol_titre:0};
          datas.push(data);
        });
        console.log("rs",resultat)
        return {datas:datas,dataBruit:resultat};
      });
      }


    ngOnInit() {
      this.getPalmares();
      setTimeout(()=>{
        this.chart2=this.AmCharts.makeChart("chartdiv2",this.makeOptions()); 
        this.getAllStructures();
      },500)
      
    }
    
  
    ngOnDestroy() {  
    
      if (this.chart2) {
        this.AmCharts.destroyChart(this.chart2);
      }
    }
    ngAfterViewInit() {
    }
    getAllStructures(){
      let dataSets:any=[];
       this.service.getStructures(1).subscribe(res=>{
        
        res.forEach((el,index)=>{
          el.lastDonnee.date=moment(el.lastDonnee.date).format("ddd DD/MM/YY");
          let dataSet:any={};
          dataSet.fieldMappings= [ {
            "fromField": "value",
            "toField": "value"
          }, {
            "fromField": "volume",
            "toField": "volume"
          } ];
          dataSet.dataProvider= /* index==0?this.chartData1:this.chartData2  */  function(){
            let datas=[];
            if(el.donnees){
            el.donnees.forEach((element,index) => {
              var firstDate = new Date(element.date);
              //firstDate.setDate( firstDate.getDate() +index );
              let data={date:firstDate,value:parseInt(element.cours_prec),volume:element.vol_titre};
              datas.push(data);
            });
          }
            return datas;
          }(); 
          dataSet.categoryField="date"; 
          dataSet.title=el.nom;
          dataSet.symbole=el.symbole;
          dataSet.description=el.description;
          dataSet.dataBruit=el.donnees;
         
         
          dataSets.push(dataSet)
          setTimeout(() => {
            this.AmCharts.updateChart(this.chart2, () => {
              this.chart2.dataSets.push(dataSet);
              this.prevDataSets=this.chart2.dataSets;
            });  
          }, 2000);
          
          //dataSets.push(dataSet);
          if(index==0){
            this.getStructureArticles(res[0].symbole);
            this.donneesCourantes=res[0];
          }
        })
        //this.structure=res[0];
        this.structures=res;
        
      });
      //return dataSets;
    }
    makeOptions(){
      return {
        "type": "stock",
        "theme": "light",
        "dataSets": [],
      
        "panels": [ {
          "showCategoryAxis": false,
          "title": "Value",
          "percentHeight": 70,
          "stockGraphs": [ {
            "id": "g1",
            "valueField": "value",
            "comparable": true,
            "compareField": "value",
            "balloonText": "[[title]]:<b>[[value]]</b>",
            "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
          } ],
          "stockLegend": {
            "periodValueTextComparing": "[[percents.value.close]]%",
            "periodValueTextRegular": "[[value.close]]"
          }
        }, {
          "title": "Volume",
          "percentHeight": 30,
          "stockGraphs": [ {
            "valueField": "volume",
            "type": "column",
            "showBalloon": false,
            "fillAlphas": 1
          } ],
          "stockLegend": {
            "periodValueTextRegular": "[[value.close]]"
          }
        } ],
      
        "chartScrollbarSettings": {
          "graph": "g1"
        },
      
        "chartCursorSettings": {
          "valueBalloonsEnabled": true,
          "fullWidth": true,
          "cursorAlpha": 0.1,
          "valueLineBalloonEnabled": true,
          "valueLineEnabled": true,
          "valueLineAlpha": 0.5
        },
      
        "periodSelector": {
          "position": "left",
          "periods": [ {
            "period": "MM",
            "selected": true,
            "count": 1,
            "label": "1 month"
          }, {
            "period": "YYYY",
            "count": 1,
            "label": "1 year"
          }, {
            "period": "YTD",
            "label": "YTD"
          }, {
            "period": "MAX",
            "label": "MAX"
          } ]
        },
      
      
        "dataSetSelector": {
          "position": "left",
          "listeners":[{event:"dataSetSelected",method:(e)=>{
            console.log(this.chart2);
            if(e.dataSet.dataProvider.length==0){
              let re:Observable<any>=this.generateChartData(e.dataSet.symbole);
              re.subscribe(data=>{
                console.log("datasetselect",data);
                e.dataSet.dataProvider=data.datas;
                e.dataSet.dataBruit=data.dataBruit;
              })
              
            }
            console.log(e.dataSet.dataBruit)
            this.getStructureArticles(e.dataSet.symbole);
          }},
          {event:"dataSetCompared",method:(e)=>{
            let re:Observable<any>=this.generateChartData(e.dataSet.symbole);
            re.subscribe(data=>{
              console.log("datasetselect",data);
              e.dataSet.dataProvider=data.datas;
              e.dataSet.dataBruit=data.dataBruit;
            })
          }}
        ],
        },
      
        "export": {
          "enabled": true
        }
        
      }
    }
    setStructure(i){
      //this.structure=this.structures[i];
       /*  this.AmCharts.updateChart(this.chart2, () => {
          this.chart2.dataSets=this.prevDataSets;
        });   */
        console.log("1",this.chart2.dataSetSelector.selectCB)
        this.updateGraph();
        console.log("2",this.chart2.dataSetSelector.selectCB)
      this.dashboardActive=true;
      let dataSets=this.chart2.dataSets;
      if(dataSets[i].dataProvider.length==0){
        let re:Observable<any>=this.generateChartData(dataSets[i].symbole);
        re.subscribe(data=>{
          this.chart2.dataSets[i].dataProvider=data.datas;
          this.chart2.dataSets[i].dataBruit=data.dataBruit;
          console.log("icii",this.chart2.dataSets[i])
        })
      }
      this.prevDataSets=this.chart2.dataSets;
      this.AmCharts.updateChart(this.chart2, () => {
        this.chart2.dataSets.map((el,index)=>{
          el.compared=false;
        })
        
        setTimeout(()=>{
          console.log(this.chart2.dataSetSelector.selectCB)
          this.chart2.dataSetSelector.selectCB[i].selected=true;
          this.chart2.mainDataSet=this.chart2.dataSets[i];
          this.chart2.validateNow();
          $('html, body').animate({scrollTop:0}, {duration:1000});
        },1000)
       
      });
      this.getStructureArticles(dataSets[i].symbole);
    }
    getStructureArticles(symbole){
      this.service.getArticlesOfStructure(symbole).subscribe(res=>{
          this.articles=res;
          this.articles.map((el,index)=>{
              let limit=100;
              if(index<=2){
                  limit=500;
              }
              if(el.contenu.length>100){
                  el.contenu=el.contenu.substring(0, limit+1);
                  el.contenu=el.contenu+"...";
                  el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
              }
          })
      })
  }
  view(id){
    this.router.navigate(['/article',id]);
}
    updateGraph(){
      this.zone.runOutsideAngular(() => {
         
        setTimeout(() => {
          this.chart2.validateNow();
          this.chart2=this.AmCharts.makeChart("chartdiv2",this.chart2);
          this.AmCharts.updateChart(this.chart2, () => {
            this.chart2.dataSets=this.prevDataSets;
          });  
        }, 1000);
      });
    }
    convertDate(d){
      return moment(d).format("ddd DD/MM/YY");
    }
    arrondVar(v){
      return Math.round(v*10000)/100;
    }
    
    arrond(v){
      return Math.round(v*100)/100;
    }
    getPalmares(){
      this.service.getPalmares().subscribe(res=>{
  
          this.palmares=res;
      })
  }
}



