import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, BrowserXhr } from '@angular/http';
import { ClarityModule } from 'clarity-angular';
import { AppComponent } from './app.component';
import { RouteReuseStrategy } from '@angular/router';
import{CustomReuseStrategy} from './custom-reuse-strategy';
// Import routing module
import { AppRoutingModule } from './app.routing';
import { HomeComponent } from "./home/home.component";
import { MarcheComponent } from "./marche/marche.component";
import { EcosystemeComponent } from "./ecosysteme/ecosysteme.component";
import { AsubTvComponent } from "./asubtv/asubtv.component";
import { ExpertsComponent } from "./experts/experts.component";
import { OpinionComponent } from "./opinion/opinion.component";
import { EvenementsComponent } from "./evenements/evenements.component";
import { EvenementComponent } from "./evenement/evenement.component";
import { ArticleComponent } from "./article/article.component";
import { VideoComponent as VideoFrame } from "./video/video.component";
import { CarouselModule } from 'ngx-bootstrap';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { NgProgressModule, NgProgressBrowserXhr } from 'ngx-progressbar';
import {ArticleComponent as articleDirec,EventComponent,VideoComponent } from './directives';
import {AsubService} from './asub.service';
import {GlobalState} from './global.state';
@NgModule({
    declarations: [
        AppComponent,
        MarcheComponent,
        EcosystemeComponent,
        OpinionComponent,
        EvenementsComponent,
        EvenementComponent,
        ArticleComponent,
        HomeComponent,ExpertsComponent,
        articleDirec,AsubTvComponent,
        EventComponent,VideoComponent,VideoFrame
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        NgProgressModule,
        ClarityModule,CarouselModule.forRoot(), AmChartsModule,
        AppRoutingModule
    ],
    providers: [{ provide: BrowserXhr, useClass: NgProgressBrowserXhr },AsubService,GlobalState,{provide: RouteReuseStrategy, useClass: CustomReuseStrategy}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
