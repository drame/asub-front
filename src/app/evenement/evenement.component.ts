/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit } from "@angular/core";
import {AsubService} from './../asub.service';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,ActivatedRoute
  } from '@angular/router'
  import * as moment from 'moment';
  import {GlobalState} from '../global.state';  
@Component({
    styleUrls: ['./evenement.component.scss'],
    templateUrl: './evenement.component.html',
})
export class EvenementComponent implements OnInit {
    public evenement:any={};
    public evenements:any=[];
    public data:any={};
    public option:any={size:2,page:0,type:"",categorie:"",pays:"",titre:""};
    public baseImageUrl;
    public pub;
constructor(public router: Router,public service:AsubService,public activatedRoute: ActivatedRoute,public globalState:GlobalState){

}

ngOnInit(){
   this.activatedRoute.params.subscribe(params=>{
       let id=params["id"];
       if(id>0){
           this.service.getEvenement(id).subscribe(res=>{
               if(res.titre){
                   this.evenement=res;
                   this.evenement.photoUrl="http://193.70.39.157:8080/asub/image?photo="+this.evenement.photoUrl;
                   this.data.id=this.evenement.id;
                   this.data.categorie=this.evenement.categorie;
                   this.data.type=this.evenement.type;
                   this.evenement.date=moment(new Date(this.evenement.date)).format("DD-MM-YYYY");
                   this.evenement.fin=moment(new Date(this.evenement.fin)).format("DD-MM-YYYY");
                   //this.getArticlesAssocie();
                  // this.getEvenementsAssocie();
               }
               else{
                   this.router.navigate(['/evenements']);
               }
           })
       }
   })
   this.globalState.subscribe('pub',data=>{
    this.pub=data;
})
}
view(id){
    this.router.navigate(['/evenement',id]);
}

getEvenementsAssocie(){
this.service.getEvenementsAssocie(this.option).subscribe(res=>{
    this.evenements=res;
    this.evenements.content.map(el=>{
            el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
    })
});
}
more(){
    this.option.size=this.option.size*2;
    this.getEvenementsAssocie();
}
}
