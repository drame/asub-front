/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit,HostListener,NgZone,ViewChild,ElementRef } from "@angular/core";
import {AsubService} from './../asub.service';
import * as moment from 'moment';
import {GlobalState} from '../global.state';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import * as $ from 'jquery';
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
  } from '@angular/router'
@Component({
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
    public articles:any={content:[]};
    public videos:any={content:[]};
    public evenements:any={content:[]};
    public option:any={size:11,page:0,types:["Article","Opinion","Entrepreneur&Startup"],categorie:"",pays:"",titre:"",id:0};
    public videosOption:any={size:4,page:0,types:["Vidéo"],categorie:"",pays:"",titre:"",id:0};
    public evenementsOption:any={size:3,page:0,type:"",categorie:"",pays:"",titre:"",lieu:""};
    public height="";
    public pub;
    public palmares={baisses:[],hausses:[]};
    public chart2: AmChart;
    public brvm10data=[];
    public brvmcdata=[];
    public taux:any=[];

constructor(public AmCharts: AmChartsService,public router: Router,public service:AsubService,public globalState:GlobalState,public zone: NgZone){
    this.router.events.subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
           this.getArticles();
          
           this.getBrvmData();
          this.getBrvmcData();
          this.getTaux();
          }
      })
      $(document).ajaxComplete(function() {
        $("a[title='JavaScript charts']").remove();
    });
}
getArticles(){
    this.service.getArticles(this.option).subscribe(res=>{
        this.articles=res;
        this.articles.content.reverse();
        this.articles.content.map((el,index)=>{
            let limit=100;
            if(index<=2){
                limit=500;
            }
            if(el.contenu.length>100){
                el.contenu=el.contenu.substring(0, limit+1);
                el.contenu=el.contenu+"...";
                el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
            }
        })
        this.getVideos();
    })
}
getVideos(){
    this.service.getArticles(this.videosOption).subscribe(res=>{
        this.videos=res;
        this.videos.content.reverse();
        this.videos.content.map((el,index)=>{
            el.photoUrl=el.photoUrl?"http://193.70.39.157:8080/asub/image?photo="+el.photoUrl:"http://img.youtube.com/vi/"+el.youtubeVideoId+"/maxresdefault.jpg";
        })

        this.getEvenements();
    })
}
getBrvmcData(){
    this.service.getAllDonneesByStructure("brvm-c").subscribe(res=>{
        this.brvmcdata=res;
    })
}
getTaux(){
    this.service.getTaux().subscribe(res=>{
        this.taux=res;
    })
}
getBrvmData(){
    this.service.getAllDonneesByStructure("brvm 10").subscribe(res=>{
            let datas=[];
            this.brvm10data=res;
            if(res){
            res.forEach((element,index) => {
              var firstDate = new Date(element.date);
              //firstDate.setDate( firstDate.getDate() +index );
              let data={date:firstDate,value:parseInt(element.cours_prec)};
              datas.push(data);
            });
          }
          
        this.chart2=this.AmCharts.makeChart("chartbrvm",{
            "type": "stock",
            "theme": "none",
            "dataSets": [ {
                "title": "first data set",
                "fieldMappings": [ {
                  "fromField": "value",
                  "toField": "value"
                }, {
                  "fromField": "volume",
                  "toField": "volume"
                } ],
                "dataProvider": datas,
                "categoryField": "date"
              }
            ],
          
            "panels": [ {
              "showCategoryAxis": false,
              "title": "Value",
              "percentHeight": 40,
              "stockGraphs": [ {
                "id": "g1",
                "valueField": "value",
                "comparable": true,
                "compareField": "value",
                "balloonText": "[[title]]:<b>[[value]]</b>",
                "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
              } ],
            
            } ],
          "chartScrollbarSettings": {
              "enabled":false ,
            },
            "chartCursorSettings": {
              "valueBalloonsEnabled": true,
              "fullWidth": true,
              "cursorAlpha": 0.1,
              "valueLineBalloonEnabled": true,
              "valueLineEnabled": true,
              "valueLineAlpha": 0.5
            },
          
          
            "export": {
              "enabled": false
            }
          });
          console.log(this.chart2);
    })
}
getEvenements(){
    this.service.getEvenements(this.evenementsOption).subscribe(res=>{
        this.evenements=res;
        this.evenements.content.map((el,index)=>{
            el.date = new Date(el.date);
            el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
        })
    })
}
more(){
    this.option.size=this.option.size*2;
    this.getArticles();
}
ngOnInit(){
    this.evenementsOption.fin=moment().format("DD-MM-YYYY");
    console.log(this.evenementsOption.fin)
   this.setHeight();
   this.globalState.subscribe('pub',data=>{
       if(data.url.indexOf('http')==-1){
           data.url='http://'+data.url;
       }
       this.pub=data;    
   })
   this.getPalmares();
   
}
view(id){
    this.router.navigate(['/article',id]);
}
play(video){
    console.log("play")
    this.globalState.notifyDataChanged("display",video);
}

getPalmares(){
    this.service.getPalmares().subscribe(res=>{

        this.palmares=res;
    })
}
arrondVar(v){
    return Math.round(v*10000)/100;
  }
  setHeight(){
    if(window.matchMedia( "(min-width: 765px)" ).matches){
        this.height="600px";
    }
    else{
        this.height="300px";
    }
}
@HostListener('window:resize', ['$event'])
onResize(event) {
    this.setHeight();
}
}
