/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit,Input,ViewEncapsulation } from "@angular/core";
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,ActivatedRoute
  } from '@angular/router';
  import * as moment from 'moment';
  moment.locale('fr');
@Component({
    selector:'asub-event',
    styleUrls: ['./event.component.scss'],
    templateUrl: './event.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class EventComponent implements OnInit {
@Input('marginBottom')
public marginBottom="";
@Input('height')
public height="";
@Input('photoUrl')
public photoUrl="";
@Input('titre')
public titre="";
@Input('description')
public description="";
@Input('classes')
public classes:any;
@Input('dateDebut')
public dateDebut:Date;
@Input('lieu')
public lieu:any;
public show=false;
constructor(){
}
mouseenter(e){
    console.log(e);
    this.show=true;
}

mouseleave(e){
    console.log(e);
    this.show=false;
}
ngOnInit(){
}
getDay(date){
return moment(date).format('ddd');
}
getMonth(date){    
return moment(date).format('MMM');
}
}
