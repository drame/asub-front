/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component,OnInit,Input,ViewEncapsulation,NgZone } from "@angular/core";
import {
    Router,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,ActivatedRoute
  } from '@angular/router'
@Component({
    selector:'asub-video',
    styleUrls: ['./video.component.scss'],
    templateUrl: './video.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class VideoComponent implements OnInit {
@Input('marginBottom')
public marginBottom="";
@Input('height')
public height="";
@Input('photoUrl')
public photoUrl="";
@Input('titre')
public titre="";
@Input('description')
public description="";
@Input('classes')
public classes:any;
@Input('pays')
public pays:any;
public show=false;
public mouseEnter=false;
constructor(public zone: NgZone){

}

ngOnInit(){
}

}
