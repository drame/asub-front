/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MarcheComponent } from './marche/marche.component';
import { HomeComponent } from './home/home.component';
import { EcosystemeComponent } from "./ecosysteme/ecosysteme.component";
import { OpinionComponent } from "./opinion/opinion.component";
import { EvenementsComponent } from "./evenements/evenements.component";
import { ArticleComponent } from "./article/article.component";
import { EvenementComponent } from "./evenement/evenement.component";
import { AsubTvComponent } from "./asubtv/asubtv.component";
import { ExpertsComponent } from "./experts/experts.component";
export const ROUTES: Routes = [
    {path: '', redirectTo: 'actualite', pathMatch: 'full'},
    {path: 'actualite', component: HomeComponent},
    {path: 'entrep_startup', component: EcosystemeComponent},
    {path: 'asubtv', component: AsubTvComponent},
    {path: 'experts', component: ExpertsComponent},
    {path: 'opinions', component: OpinionComponent},
    {path: 'evenements', component: EvenementsComponent},
    {path: 'article/:id', component: ArticleComponent},
    {path: 'evenement/:id', component: EvenementComponent},
    {path: 'marche', component: MarcheComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(ROUTES, { useHash: true }) ],
    exports: [ RouterModule ]
  })
  export class AppRoutingModule {}
